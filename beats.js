const scribble = require('scribbletune');

const cords = [ 'maj',
  'min',
  'sus2',
  'sus4',
  'maj7',
  'min7',
  'dom7',
  'dim',
  'dim7',
  'aug',
  'sixth',
  'Maj',
  'm',
  'Min',
  'Dim',
  'Dim7',
  'Maj7',
  'Min7',
  'm7',
  '7th',
  'Dom7',
  'Sus2',
  'Sus4',
  'Aug',
  '6th',
  'Sixth' ]

let clip = scribble.clip({
    notes: ['c3', 'd3', 'd3'],
    pattern: 'x--xx---'.repeat(4),
});

let clip2 = clip.reverse();

let clip3 = scribble.clip({
    notes: ['d2', 'd2', 'e2', 'd3', 'd3', 'e3'],
    pattern: 'x--xx---'.repeat(8),
    sizzle: true

});

let clip4 = scribble.clip({
    notes: ['d4', 'd4', 'e4', 'd3', 'd3', 'e4'],
    pattern: 'x--xx---'.repeat(4),
    sizzle: true
});
console.log('scribble.cords()', scribble.listChords());
clip.concat(clip2);
clip = clip.concat(clip3);

scribble.midi(clip);

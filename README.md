# A [ScribbleTune](https://github.com/walmik/scribbletune) Scaffold

## Requirements:
  - [node](http://nodejs.org)

## Optional Requirements:
  - [timidity](http://linux-audio.com/TiMidity-howto.html)
  - [nodemon](https://github.com/remy/nodemon)
## Scripts

*Look in package.json (under scripts) for these commands.*

  - `node run watch`
    - runs a watcher that runs your code and plays music.mid automatically
    - requires nodemon and timidity
  - `node run watch-silent`
    - runs a watcher that runs your code automatically
    - requires nodemon
  - `node run start`:
    - runs your code once.
    - same as `node beats.js`